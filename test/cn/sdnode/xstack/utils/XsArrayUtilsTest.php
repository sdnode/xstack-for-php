<?php
namespace cn\sdnode\xstack\utils;
use PHPUnit\Framework\TestCase;
use cn\sdnode\xstack\utils\XsArrayUtils;

class XsArrayUtilsTest extends TestCase
{
    public function testLength()
    {
        $this->assertEquals(3, XsArrayUtils::length(['a', 'b', 'c']));
    }

    // 比较和检查
    public function testIsEquals()
    {
        $this->assertTrue(XsArrayUtils::isEquals(['a', 'b', 'c'], ['a', 'b', 'c']));
        $this->assertFalse(XsArrayUtils::isEquals(['a', 'b'], ['a', 'b', 'c']));
    }

    public function testContains()
    {
        $this->assertTrue(XsArrayUtils::contains(['a', 'b', 'c'], 'b'));
        $this->assertFalse(XsArrayUtils::contains(['a', 'b', 'c'], 'd'));
    }

    public function testIsEmpty()
    {
        $this->assertTrue(XsArrayUtils::isEmpty([]));
        $this->assertFalse(XsArrayUtils::isEmpty([0]));
    }


    // 索引查找
    public function testIndexOf()
    {
        $this->assertEquals(1, XsArrayUtils::indexOf(['a', 'b', 'c'], 'b'));
        $this->assertEquals(-1, XsArrayUtils::indexOf(['a', 'b', 'c'], 'd'));
    }

    public function testIndexesOf()
    {
        $this->assertEquals([1,3], XsArrayUtils::indexesOf(['a', 'b', 'c', 'b'], 'b'));
        $this->assertEquals([], XsArrayUtils::indexesOf(['a', 'b', 'c'], 'd'));
    }

    // 索引查找
    public function testIndexOfByObjectKey()
    {
        $this->assertEquals(1, XsArrayUtils::indexOfByObjectKey([["id"=>1], ["id"=>2]], 'id', '2'));
        $this->assertEquals(-1, XsArrayUtils::indexOfByObjectKey([["id"=>1], ["id"=>2]], 'id', '3'));
    }

    public function testIndexesOfByObjectKey()
    {
        $this->assertEquals([1, 2], 
            XsArrayUtils::indexesOfByObjectKey([["id"=>1], ["id"=>2], ["id"=>2]], 'id', 2));
        $this->assertEquals([], 
            XsArrayUtils::indexesOfByObjectKey([["id"=>1], ["id"=>2], ["id"=>2]], 'id', 3));
    }    


    // 子数组操作
    public function testGetSubArray()
    {
        $array = ['a', 'b', 'c', 'd', 'e'];
        $this->assertEquals(['b', 'c'], XsArrayUtils::getSubArray($array, 1, 3));
    }

    // 数组修改和添加
    public function testAdd()
    {
        $array = ['a', 'b', 'c'];
        XsArrayUtils::add($array, 'd');
        $this->assertEquals(['a', 'b', 'c', 'd'], $array);
    }

    public function testAddArray()
    {
        $array = ['a', 'b', 'c'];
        XsArrayUtils::addArray($array, ['d', 'e']);
        $this->assertEquals(['a', 'b', 'c', 'd', 'e'], $array);
    }

    public function testInsert()
    {
        $array = ['a', 'c', 'd'];
        XsArrayUtils::insert($array, 'b', 1);
        $this->assertEquals(['a', 'b', 'c', 'd'], $array);
    }

    public function testRemoveElement()
    {
        $array = ['a', 'b', 'c'];
        XsArrayUtils::removeElement($array, 'b');
        $this->assertEquals(['a', 'c'], $array);
    }


    public function testRemove()
    {
        $array = [1, 2, 3, 4, 5];
        XsArrayUtils::remove($array, 1, 2);
        $this->assertSame([2, 3], $array);
    }

    
    public function testReverse()
    {
        $array = ['a', 'b', 'c'];
        XsArrayUtils::reverse($array);
        $this->assertEquals(['c', 'b', 'a'], $array);
    }
 
    public function testClone()
    {
        $array = ['a', 'b', 'c'];
        $cloned = XsArrayUtils::clone($array);
        $this->assertEquals($array, $cloned);
    }

    public function testToString()
    {
        $array = ['Hello', 'World'];
        $this->assertEquals('Hello, World', XsArrayUtils::toString($array));
    }


    public function testStackPush()
    {
        $array = ['a', 'b'];
        XsArrayUtils::stackPush($array, 'c');
        $this->assertSame(['a', 'b', 'c'], $array);
    }

    public function testStackPop()
    {
        $array = ['a', 'b', 'c'];
        $item = XsArrayUtils::stackPop($array);
        $this->assertSame(['a', 'b'], $array);
        $this->assertSame('c', $item);
    }

    public function testQueuePush()
    {
        $array = ['b', 'c'];
        XsArrayUtils::queuePush($array, 'a');
        $this->assertSame(['a', 'b', 'c'], $array);
    }

    public function testQueuePop()
    {
        $array = ['a', 'b', 'c'];
        $item = XsArrayUtils::queuePop($array);
        $this->assertSame(['a', 'b'], $array);
        $this->assertSame('c', $item);
    }
}
