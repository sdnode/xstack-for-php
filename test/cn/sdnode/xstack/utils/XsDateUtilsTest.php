<?php
namespace cn\sdnode\xstack\utils;
use DateTime;
use PHPUnit\Framework\TestCase;
use cn\sdnode\xstack\utils\XsDateUtils;

class XsDateUtilsTest extends TestCase
{

    const TEST_DATE_STR = '2024-05-27';
    const TEST_TIMESTAMP = 1716739200;

    public function testStringToTimestamp()
    {
        $this->assertSame(
            self::TEST_TIMESTAMP,
            XsDateUtils::stringToTimestamp(self::TEST_DATE_STR)
        );
    }

    public function testStringToDate()
    {
        $datetime = XsDateUtils::stringToDate(self::TEST_DATE_STR);
        $this->assertEquals($datetime, new DateTime(self::TEST_DATE_STR));
    }

    public function testTimestampToString()
    {
        $dateStr = XsDateUtils::timestampToString(self::TEST_TIMESTAMP);
        $this->assertSame(self::TEST_DATE_STR, $dateStr);
    }

    public function testDateToString()
    {
        $testDate = new DateTime(self::TEST_DATE_STR);
        $this->assertSame(self::TEST_DATE_STR, XsDateUtils::dateToString($testDate));
    }



    public function testGetYear()
    {
        $this->assertEquals(2024, XsDateUtils::getYear('2024-01-01'));
    }

    public function testGetMonth()
    {
        $this->assertEquals(1, XsDateUtils::getMonth('2024-01-01'));
    }

    public function testGetMonthDay()
    {
        $this->assertEquals(1, XsDateUtils::getMonthDay('2024-01-01'));
    }

    public function testGetWeek()
    {
        $this->assertEquals(1, XsDateUtils::getWeek('2024-01-01'));
    }

    public function testGetWeekday()
    {
        $this->assertEquals(1, XsDateUtils::getWeekday('2024-01-01'));
    }

    public function testParse()
    {
        $this->assertEquals(new DateTime('2024-01-01'), XsDateUtils::parse('2024-01-01'));
        $this->assertEquals(new DateTime('2024-01-01'), XsDateUtils::parse('2024/01/01'));
    }

    public function testFormat()
    {
        $this->assertEquals('2024-01-01', XsDateUtils::format(new DateTime('2024-01-01')));
        $this->assertEquals('2024/01/01',XsDateUtils::format(new DateTime('2024-01-01'),'yyyy/MM/dd'));
    }

    public function testAddDays()
    {
        $this->assertEquals(
            '2024-03-05',
            XsDateUtils::addDays('2024-03-01', 4)
        );
    }

    public function testAddWeeks()
    {
        $this->assertEquals(
            '2024-03-22',
            XsDateUtils::addWeeks('2024-03-01', 3)
        );
    }

    public function testAddMonths()
    {
        $this->assertEquals(
            '2024-05-01',
            XsDateUtils::addMonths('2024-03-01', 2)
        );
    }

    public function testAddYears()
    {
        $this->assertEquals(
            '2025-03-01',
            XsDateUtils::addYears('2024-03-01', 1)
        );
    }


    public function testSubDateYears()
    {
        $this->assertSame(2, XsDateUtils::subDateYears('2019-12-31', '2021-12-31'));
        $this->assertSame(0, XsDateUtils::subDateYears('2020-01-01', '2020-12-31'));
    }

    public function testSubDateMonths()
    {
        $this->assertSame(12, XsDateUtils::subDateMonths('2020-01-01', '2021-01-01'));
        $this->assertSame(0, XsDateUtils::subDateMonths('2020-05-15', '2020-05-31'));
    }

    public function testSubDateDays()
    {
        $this->assertSame(366, XsDateUtils::subDateDays('2020-01-01', '2021-01-01'));
        $this->assertSame(30, XsDateUtils::subDateDays('2020-05-01', '2020-05-31'));
    }

    public function testGetIntervalDate()
    {
        // 基本测试
        $this->assertSame(['2024-01-01', '2024-01-02', '2024-01-03'], XsDateUtils::getIntervalDates('2024-01-01', '2024-01-03'));
        // 测试包含闰年2月的日期
        $this->assertSame(['2024-02-28', '2024-02-29', '2024-03-01'], XsDateUtils::getIntervalDates('2024-02-28', '2024-03-01'));
        // 测试结束日期等于开始日期
        $this->assertSame(['2024-01-01'], XsDateUtils::getIntervalDates('2024-01-01', '2024-01-01'));
        // 测试开始日期晚于结束日期的情况（期望返回空数组）
        $this->assertSame([], XsDateUtils::getIntervalDates('2024-01-03', '2024-01-01'));
    }

    public function testGetDatesFromSepdate()
    {
        // 测试基本功能
        $this->assertSame(['2024-01-05', '2024-01-04', '2024-01-03', '2024-01-06'], XsDateUtils::getDatesFromSepdate('2024-01-05', 2, 1));
        // 测试没有扩展天数的情况
        $this->assertSame(['2024-01-05'], XsDateUtils::getDatesFromSepdate('2024-01-05', 0, 0));
    }
    
}
